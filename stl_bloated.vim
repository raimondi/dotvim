" Functions {{{1

" Moon phase and paste flag for the statusline. Weird, eh? {{{2
function! Moon()
  " Show moon phase:
  " Phase of the Moon calculation
  let time = localtime()
  let fullday = 86400
  let offset = 592500
  let period = 2551443
  let phase = (time - offset) % period
  let phase = phase / fullday
  return printf("[%d]", phase)
endfunction

" Buffer size: {{{2
function! FileSize()
  let bytes = wordcount().bytes
  if bytes <= 0
    let number = 0
    let sufix = ''
  elseif bytes < 1024
    let number = bytes
    let sufix = ''
  elseif bytes < 1024 * 1024
    let number = bytes / 1024.0
    let sufix = "K"
  elseif bytes < 1024 * 1024 * 1024
    let number = bytes / (1024 * 1024.0)
    let sufix = "M"
  elseif bytes < 1024 * 1024 * 1024 * 1024
    let number = bytes / (1024 * 1024 * 1024.0)
    let sufix = "G"
  else
    let number = bytes / (1024 * 1024 * 1024 * 1024.0)
    let sufix = 'WTF?!'
  endif
  return printf('%3.1f%s', number, sufix)
endfunction

" Show info about the char under the cursor: {{{2
function! CharUnderCursorInfo()
  let char = char2nr(strcharpart(getline('.')[col('.') - 1:], 0, 1))
  return printf('%d,#%x,0%o', char, char, char)
endfunction

" Add flag and highlight wrong values: {{{2
" Add the variable with the name a:varName to the statusline. Highlight it as
" 'error' unless its value is in a:goodValues (a comma separated string)
function! AddStatuslineFlag(varName, goodValues, prefix, sufix)
  exec 'set statusline+='.a:prefix
  exec "set statusline+=%{RenderStlFlag(".a:varName.",'".a:goodValues."',1)}"
  set statusline+=%*
  exec "set statusline+=%{RenderStlFlag(".a:varName.",'".a:goodValues."',0)}"
  exec 'set statusline+='.a:sufix
endfunction

" Render stl flag: {{{2
"
" returns a:value or ''
"
" a:goodValues is a comma separated string of values that shouldn't be
" highlighted with the error group
"
" a:error indicates whether the string that is returned will be highlighted as
" 'error'
"
function! RenderStlFlag(value, goodValues, error)
  let goodValues = split(a:goodValues, ',')
  let good = index(goodValues, a:value) != -1
  if (a:error && !good) || (!a:error && good)
    return a:value
  else
    return ''
  endif
endfunction

" Indicate tabstop value: {{{2
"execute 'set statusline+=,' . nr2char(187) . '–%{&tabstop}'
function! IndentStatus(prefix, sufix)
  let str = a:prefix
  if !&et
    let str .= nr2char(187) . '–' . &shiftwidth . '/' . &tabstop
  else
    let str .= nr2char(183) . nr2char(183) . &shiftwidth . '/' . &tabstop
  endif
  "let str .= ':'.(indent('.')/&sw + 1)
  let str .= a:sufix
  return str
endfunction

" Warn on mixed indenting and wrong et value: {{{2
" return '[&et]' if &et is set wrong
" return '[mixed-indenting]' if spaces and tabs are used to indent
" return an empty string if everything is fine
let b:statusline_tab_warning = ''
function! StatuslineTabWarning()
  if (wordcount().bytes >= 20*1024*1024 || &ft =~? 'help')
    return ''
  elseif exists("b:statusline_tab_warning")
    return b:statusline_tab_warning
  endif
  let pos = getpos('.')
  1
  let tabs = search('^\s*\t', 'W')
  while tabs
        \&& join(map(
        \  synstack(line('.'),col('.')),
        \  'synIDattr(v:val,"name")')) =~? 'string\|comment'
    let tabs = search('^\%>'.tabs.'l\s*\t', 'W')
  endwhile
  1
  let spaces = search('^\t* ', 'W')
  while spaces
        \&& join(map(
        \  synstack(line('.'),col('.')),
        \  'synIDattr(v:val,"name")')) =~? 'string\|comment'
    let spaces = search('^\%>'.spaces.'l\t', 'W')
  endwhile
  call setpos('.', pos)
  if (tabs && spaces) && (&filetype != "help")
    let b:statusline_tab_warning =  '[mixed-indenting]'
  elseif (spaces && !&et) || (tabs && &et)
    let b:statusline_tab_warning = '[wrong-indenting]'
  else
    let b:statusline_tab_warning = ''
  endif
  return b:statusline_tab_warning
endfunction

" Warn on "long lines": {{{2
"return a warning for "long lines" where "long" is either &textwidth or 80 (if
"no &textwidth is set)
"
"return '' if no long lines
"return '[#x,my,$z] if long lines are found, were x is the number of long
"lines, y is the median length of the long lines and z is the length of the
"longest line
function! StatuslineLongLineWarning(...)
  if wordcount().bytes >= 20*1024*1024 || &ft =~? 'netrw\|help'
    let b:statusline_long_line_warning = ""
  elseif !exists("b:statusline_long_line_warning")
    let long_line_lens = call('s:LongLines', a:000)
    if len(long_line_lens) > 0
      let b:statusline_long_line_warning = "[" .
            \ '#' . len(long_line_lens) . "," .
            \ 'm' . s:Median(long_line_lens) . "," .
            \ '$' . max(long_line_lens) .
            \ ']'
    else
      let b:statusline_long_line_warning = ""
    endif
  endif
  return b:statusline_long_line_warning
endfunction

"return a list containing the lengths of the long lines in this buffer {{{2
function! s:LongLines(...)
  let threshold = &tw ? &tw : get(a:000, 0, 80)
  let spaces = repeat(" ", &ts)
  let long_line_lens =
        \map(getline(1,'$'), 'len(substitute(v:val, "\\t", spaces, "g"))')
  return filter(long_line_lens, 'v:val > threshold')
endfunction

"find the median of the given array of numbers {{{2
function! s:Median(nums)
  let nums = sort(a:nums)
  let l = len(nums)
  if l % 2 == 1
    let i = (l-1) / 2
    return nums[i]
  else
    return (nums[l/2] + nums[(l/2)-1]) / 2
  endif
endfunction

" Warn on trailing spaces: {{{2
" return '[\s]' if trailing white space is detected
" return '' otherwise
let b:statusline_trailing_space_warning = ''
function! StatuslineTrailingSpaceWarning()
  if wordcount().bytes >= 20*1024*1024 || &ft =~? 'netrw\|help'
    let b:statusline_trailing_space_warning = ''
  elseif !exists("b:statusline_trailing_space_warning")
    if search('\s\+$', 'nw')
      let b:statusline_trailing_space_warning = '[\s$]'
    else
      let b:statusline_trailing_space_warning = ''
    endif
  endif
  return b:statusline_trailing_space_warning
endfunction

" Autocmds {{{1

augroup statusline
  autocmd!
  " Recalculate the tab warning flag when idle and after writing
  autocmd CursorHold,BufWritePost * unlet! b:statusline_tab_warning

  " Recalculate the long line warning when idle and after saving
  autocmd CursorHold,BufWritePost * unlet! b:statusline_long_line_warning

  " Recalculate the trailing whitespace warning when idle, and after saving
  autocmd CursorHold,BufWritePost * unlet! b:statusline_trailing_space_warning

augroup END

" Status Line {{{1
" -----------

" Reset status line:
set statusline=

" Default color:
set statusline+=%0*

set statusline+=%<


" File path:
"set statusline+=%#identifier#
set statusline+=%f
"set statusline+=%*

" Modified flag:
set statusline+=%#warningmsg#
set statusline+=%(\ %M\ %)

set statusline+=%*

" File size:
"set statusline+=%#identifier#
set statusline+=(%{FileSize()})
"set statusline+=%*

" Buffer number:
"set statusline+=%#identifier#
set statusline+=:%n/
"set statusline+=%*

" Number of buffers:
"set statusline+=%#identifier#
set statusline+=%{len(filter(range(1,bufnr('$')),'buflisted(v:val)'))}
"set statusline+=%*


"Indent settings
"set statusline+=%#identifier#
set statusline+=[%{IndentStatus(',','')}]

"set statusline+=[

" Type of  file flag:
set statusline+=%y

" Help flag:
"set statusline+=%#identifier#
"set statusline+=%H

" Preview window flag:
"set statusline+=%#identifier#
set statusline+=%w

" Use error color:
set statusline+=%#warningmsg#

" Readonly flag:
set statusline+=%r

" Return to default color:
set statusline+=%*

"display a warning if fileformat isnt unix
set statusline+=%#warningmsg#
set statusline+=%{&ff!='unix'?'['.&ff.']':''}
set statusline+=%*

" Alert me if file encoding is not UTF-8:
set statusline+=%#warningmsg#
set statusline+=%{(&fenc!='utf-8'&&&fenc!='')?'['.&fenc.']':''}
set statusline+=%*

" display a warning if &et is wrong, or we have mixed-indenting
set statusline+=%#warningmsg#
set statusline+=%{StatuslineTabWarning()}
set statusline+=%*

" puts the trailing spaces flag on the statusline
set statusline+=%#warningmsg#
set statusline+=%{StatuslineTrailingSpaceWarning()}
set statusline+=%*

" Alert me of long lines:
set statusline+=%#warningmsg#
set statusline+=%{StatuslineLongLineWarning(90)}
set statusline+=%*

"set statusline+=]

"display a warning if &paste is set
set statusline+=%#error#
set statusline+=%{&paste?'[paste]':''}
set statusline+=%*

" Start right aligned items:
set statusline+=\ %=

" Display date and time:
"set statusline+=\(%{strftime(\"%D\ %T\",getftime(expand(\"%:p\")))}\)

" Display moon face:
"set statusline+=\ Moon:%{Moon()}

" Show decimal, hexadecimal and octal values of char under the cursor.
"set statusline+=[%{CharUnderCursorInfo()}]
set statusline+=[ga]

" Indent level. No indentation is level 1
"set statusline+=%{(indent('.')/&sw+1).'\|'}

" Hex representation of ñchar under the cursor:
"set statusline+=0x\%B,

" Line number
set statusline+=%l:

" Column number:
set statusline+=%c

" Virtual column number:
set statusline+=%V

" Percentage through file/Number of lines in buffer:
set statusline+=\ %P/%L

" vim: set et sw=2:
