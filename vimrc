" Options: {{{1
""""""""""
" Use :help 'option (including the ' character) to learn more about each one.
"
set nocompatible " NEVER change this! Use Vim mode, not vi mode.

set shell=sh

" If this is on, it might interfere with pathogen:
filetype off

" Let pathogen perform its magic:
source ~/.vim/bundle/pathogen/autoload/pathogen.vim

" Set runtimepath
exe pathogen#infect('bundle', 'test')
"call pathogen#runtime_append_all_bundles('test')

" Search upwards for the tags file. :h file-searching
set tags=./tags;

filetype plugin indent on " Enable automatic settings based on file type
if !exists('g:syntax_on')
  syntax enable           " Enable color syntax highlighting
endif

set hidden  " Edit multiple unsaved files at the same time
set confirm " Prompt to save unsaved changes when exiting
"set nomore  " Don't ask me to show more output

" Keep various histories between edits
set viminfo='1000,f1,<500,:100,/100,h
set history=1000

if has('multibyte')
  set encoding=utf-8
endif

" Search Options:
set hlsearch   " Highlight searches. See below for more.
set ignorecase " Do case insensitive matching...
set smartcase  " ...except when using capital letters
set incsearch  " Incremental search

" Insert Edit Options:
set backspace=indent,eol,start " Better handling of backspace key
set formatoptions-=r " Do not insert a comment leader after hitting <Enter>
set formatoptions+=j

set nostartofline     " Emulate typical editor navigation behaviour
set nopaste           " Start in normal (non-paste) mode
set pastetoggle=<f11> " Use <F11> to toggle between 'paste' and 'nopaste'
"set nrformats+=alpha  " make Ctrl-A/Ctrl-x work on alphabetic chars.

" Status And Command Line Options:
set wildmenu                   " Better commandline completion
set wildmode=list:longest,full " Expand match on first Tab complete
set wildchar=<Tab>
set showcmd      " Show (partial) command in status line.
set showmode     " Display the current mode
set laststatus=2 " Always show a status line
set cmdheight=2  " Prevent "Press Enter" message after most commands
" Show detailed information in status line
runtime stl_bloated.vim

" Interface Options:
set display=lastline
set background=dark " Use a dark background
"set background=light " Use a dark background
set number           " Display line numbers at left of screen
"set rnu              " Display relative to the cursor line numbers
set visualbell       " Flash the screen instead of beeping on errors
set t_vb=            " And then disable even the flashing
set mouse=a          " Enable mouse usage (all modes) in terminals
set scrolloff=2      " Number of screen lines to show around the cursor
set updatetime=300
"set backupdir=~/.vim/backup
"set directory=~/.vim/tmp//
set lazyredraw

" Quickly time out on keycodes, but never time out on mappings
set timeout ttimeout ttimeoutlen=200
set wrap
set linebreak

" Catch trailing whitespace
set listchars=
" set listchars=tab:>-,trail:·
if has('multibyte') && &encoding == 'utf-8'
  exec 'set listchars+=tab:' . nr2char(9656) . '\ ,trail:' . nr2char(9608)
        \ . ',eol:' . nr2char(172)
else
  set listchars+=tab:\|_,trail:·
endif
set splitright " Open new split windows on the right side.

" have the h, l and cursor keys wrap between lines, as well as <Space> and
" <BackSpace>, and ~ convert case over line breaks; also have the cursor keys
" wrap in insert mode:
set whichwrap=b,s,h,l,~,<,>,[,]

" Indentation Options:
set tabstop=8 " NEVER change this!
" Change the '2' value below to your preferred indentation level
set shiftwidth=2 softtabstop=0 " Number of spaces for each indent level
set expandtab " Even when pressing <Tab>
set autoindent " Sane indenting when filetype not recognised
set shiftround " Always use a multiple of shiftwidth.
if exists('&breakindent')
  set breakindent
  set breakindentopt=shift:4
  set showbreak=
endif

" Persistent undo:
if has('persistent_undo')
    set undofile
endif

" Use ack instead of grep
if executable('rg')
  set grepprg=rg\ --vimgrep\ --no-heading
  set grepformat=%f:%l:%c:%m,%f:%l:%m
elseif executable('ag')
  set grepprg=ag\ --nogroup\ --nocolor
elseif executable('ack')
  set grepprg=ack\ $@\ --column
  set grepformat=%f:%l:%c:%m
endif

if exists('&splitvertical')
  set nosplitvertical
endif

" Define mapleaders.
let mapleader      = '-'
let maplocalleader = '_'

" Set terminal title
set title

" tmux will only forward escape sequences to the terminal if surrounded by a DCS sequence
" http://sourceforge.net/mailarchive/forum.php?thread_name=AANLkTinkbdoZ8eNR1X2UobLTeww1jFrvfJxTMfKSq-L%2B%40mail.gmail.com&forum_name=tmux-users

if !has('gui_running') && $TERM_PROGRAM == 'iTerm.app'
  " Change cursor shape
  " 0: Block
  " 1: Vertical bar
  " 2: Underline
  if exists('$TMUX')
    "let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
    "let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=2\x7\<Esc>\\"
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_EI = "\<Esc>]50;CursorShape=2\x7"
    set <HOME>=OH
    set <END>=OF
  else
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_EI = "\<Esc>]50;CursorShape=2\x7"
  endif
endif

" Folding:
set foldenable
set foldcolumn=2
"set foldlevel=99
set foldmethod=marker

" Fix mouse issues in iTerm2 with wide windows.
if $TERM_PROGRAM == 'iTerm.app'
  if has('termguicolors')
    set termguicolors
    if !empty($TMUX)
      set t_8f=[38;2;%lu;%lu;%lum
      set t_8b=[48;2;%lu;%lu;%lum
    endif
  endif
  if has('mouse_sgr') && !has('gui_running')
    set ttymouse=sgr
  endif
endif

"""""""
" Maps: {{{1
"""""""

" Reverse of CR
inoremap <S-CR> <Up><End><CR>

" Wrap On:
"nnoremap <expr>k &wrap ? 'gk' : 'k'
"nnoremap <expr>j &wrap ? 'gj' : 'j'
"nnoremap <expr>A &wrap ? 'g$a' : 'A'
"nnoremap <expr>I &wrap ? 'g^i' : 'I'
"nnoremap <expr>0 &wrap ? 'g0'  : '0'
"nnoremap <expr>^ &wrap ? 'g^'  : '^'
"nnoremap <expr>$ &wrap ? 'g$'  : '$'

" map Y to act like d and c, i.e. to yank until eol, rather than act as yy,
" which is the default
noremap Y y$

" map <c-l> (redraw screen) to also turn off search highlighting until the
" next search
"nnoremap <c-l> :nohl<cr><c-l><plug>mashunmash

" toggle search highlighting
"nnoremap <c-bslash> :set hls!<bar>:set hls?<cr>
"inoremap <c-bslash> <esc>:set hls!<bar>:set hls?<cr>a

" assume the /g flag on :s substitutions to replace all matches in a line:
"set gdefault
"map gs :%s//&/g<left><left><left><left>

" always edit a file on gf, but be nice and ask
noremap gf :<C-U>call GoFile()<cr>

" move between windows:
"nnoremap <silent> <c-j> <c-w>j
"nnoremap <silent> <c-k> <c-w>k
"nnoremap <silent> <c-l> <c-w>l
"nnoremap <silent> <c-h> <c-w>h

" scrolling:
nnoremap <c-up> k<c-y>
nnoremap <c-down> j<c-e>

" tabular:
nnoremap <Leader>a= :Tabularize /^[^=]*\zs=<CR>
vnoremap <Leader>a= :Tabularize /^[^=]*\zs=<CR>
nnoremap <Leader>a: :Tabularize /:\zs<CR>
vnoremap <Leader>a: :Tabularize /:\zs<CR>

" Prevent me from typing nbs
inoremap   <C-O>:echo 'That was a NBS you idiot!'<CR>

" Clean window
"nnoremap <silent> <C-l> :nohl<CR><C-l>

" Nicer ls
nnoremap <leader>b :<C-U>ls<CR>:b<Space>

noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 20, 4)<CR>
noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 20, 4)<CR>
noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 20, 4)<CR>
noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 20, 4)<CR>

" Make U do something useful
nnoremap U :<C-U>echoerr "You just entered U, is CAPSLOCK ON?, Idiot!"<CR>

nnoremap <Leader>si :echo <SID>SynInfo()<CR>
if exists('*synstack')
  nnoremap <leader>ss :<C-U>echo join(map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")'), ' => ')<CR>
endif

"cnoremap <expr> <CR> CCR()

""""""""""""""""
" Auto commads {{{1
""""""""""""""""
"
if has("autocmd")
  augroup vimrc
    " Remove ALL autocommands for the current group.
    autocmd!

    " No warning about more files to edit
    " https://www.reddit.com/r/vim/comments/1vqj4l/is_it_possible_to_silence_e173_n_more_files_to/ceuuu3p/
    au VimEnter * if argc() | silent bprev | silent bnext | endif

    " Enable omni-completion by default
    if has("autocmd") && exists("+omnifunc")
      autocmd Filetype *
            \   if &omnifunc == "" |
            \       setlocal omnifunc=syntaxcomplete#Complete |
            \   endif
    endif

    " Enable extended % matching
    au VimEnter * au FileType * if !exists("b:match_words")
          \ | let b:match_words = &matchpairs | endif

    " Don't write backup file if vim is being called by "crontab -e"
    "au BufWrite crontab.* set nowritebackup
    au BufEnter crontab.* setl backupcopy=yes

    " Jump to last-known-position when editing files
    " Note: The | character is used in Vim as a command separator (like ; in
    " C)
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$")
          \ && expand('%:t') !=? 'commit_editmsg' | exe "normal! g`\"" |
          \ endif

    "au BufEnter * set rnu

    au FileType lisp let b:delimitMate_balance_matchpairs = 0 |
          \ let b:delimitMate_quotes = '" *'

    au BufEnter * if index(['startify', 'help', ''], &ft) == -1 | call matchadd('ErrorMsg', '\s\+$') | endif

    " Use Tabular if available.
    au FileType vimpeg silent! ino <buffer><expr> :
          \ getline('.')[:col('.')] =~ '^\s*\h\w*\s*[^:]*$'
          \ ? "::= " . (exists('g:tabular_loaded')
          \   ? "\<C-O>:Tabularize/^[^:]*\\zs::=\<CR>\<C-O>f=\<Del>= "
          \   : '')
          \ : ':'

    " allow leaving cmdline-window with <ESC>
    "au CmdwinEnter * nnoremap <buffer> <ESC> :q<cr>

    au InsertLeave,BufWritePre bike_log.csv let s:pos = getpos('.')
          \ |exec 'Tabularize /|/'
          \ |call setpos('.', s:pos)
    au BufEnter bike_log.csv command! -buffer  Export enew |
          \ silent 0read # | $d | silent %s/"/""/ge |
          \ silent %s/\s\+|\s\+/","/g | silent %s/^\|$/"/g |
          \ call feedkeys(':write ', 'n') |
          \ ino <buffer><silent> <bar>
          \   <bar><C-O>:Tabularize /<bar>/<CR><Esc>A

    au BufRead,BufNewFile Portfile set ft=tcl
  augroup END
endif

"""""""""""""""""""

" Plugins settings {{{1
"""""""""""""""""""
" do-too
let g:dotoo#capture#refile = expand('~/Documents/refile.dotoo')

" Switch
let switch_custom_definitions =
      \ [
      \   {
      \     '^\s*fu\%[nction]!\@!': '\0!',
      \     '\(^\s*fu\%[nction]\)!': '\1',
      \   }
      \ ]

"let $PYTHONHOME = '/opt/local/'
let g:loaded_vimpreviewtag = 1

" Buffalo:
"let g:buffalo_trigger = "\<C-Y>"
let buffalo_buffer_numbers = 1
let buffalo_pretty = 1
let buffalo_autoaccept = 1

" Enable extended % matching
runtime macros/matchit.vim

" Templates:
let username = 'Israel Chauca F.'
let email = 'israelchauca@gmail.com'

"Edit file in existing vim automatically
"runtime! macros/editexisting.vim

if filereadable(glob("~/bin/ctags"))
  let Tlist_Ctags_Cmd = "~/bin/ctags"
endif
if !empty(glob("/opt/local/bin/ctags", 1, 1))
  let tagbar_ctags_bin = "/opt/local/bin/ctags"
endif

" Snippets:
let g:snips_author = 'Israel Chauca Fuentes'

let g:UltiSnipsExpandTrigger='<tab>'
let g:UltiSnipsJumpForwardTrigger='<C-N>'
let g:UltiSnipsJumpBackwardTrigger='<C-P>'

" Where to find rgb.txt:
let g:rgbtxt = expand("$HOME/.vim/rgb.txt")

" CSApprox for Terminal.app
" set t_Co=256
let g:CSApprox_verbose_level = 0

"don't load csapprox if we have no gui support - silences an annoying warning
if !has("gui")
  let g:CSApprox_loaded = 1
endif

" NERDCommenter makes too much noise:
let g:NERDShutUp = 1

" NetRW
let netrw_banner = 0
let netrw_browse_split = 4 " Open in prior window
let netrw_altv = 1         " Open splits on the right
let netrw_liststyle = 3        " Tree view


" NERDTree:
let NERDTreeChDirMode = 2
let NERDTreeHijackNetrw = 0

" DelimitMate:
"let delimitMate_autoclose = 0
"let loaded_delimitMate = 1
"let delimitMate_testing = "fork"
"let delimitMate_excluded_ft = "mailapp"
let delimitMate_expand_space = 1
let delimitMate_expand_cr = 1
let delimitMate_balance_matchpairs = 1
let delimitMate_nesting_quotes = ['"']
"let g:delimitMate_matchpairs = '(:),{:},[:],<:>,¿:?,¡:!'
"let delimitMate_eol_marker = ';'
let delimitMate_jump_expansion = 1
au FileType test let b:delimitMate_expand_cr = 1

" Gist:
let gist_clip_command = 'pbcopy'
let gist_open_browser_after_post = 1
let g:gist_put_url_to_clipboard_after_post = 1

" Tagslist:
" Ant build file:
let g:tlist_ant_settings = 'ant;p:Project;t:Target'
" TagList Settings {
let Tlist_Auto_Open=0 " let the tag list open automagically
let Tlist_Compact_Format = 1 " show small menu
let Tlist_Ctags_Cmd = '~/bin/ctags' " location of ctags
let Tlist_Enable_Fold_Column = 0 " do show folding tree
let Tlist_Exist_OnlyWindow = 1 " if you are the last, kill
" yourself
let Tlist_File_Fold_Auto_Close = 0 " fold closed other trees
let Tlist_Sort_Type = "name" " order by
let Tlist_Use_Right_Window = 1 " split to the right side
" of the screen
let Tlist_WinWidth = 40 " 40 cols wide, so i can (almost always)
" read my functions

" Tagbar:
"let tagbar_ctags_bin = '/usr/local/bin/ctags'

" VimRegEx:
let g:VimrexSrchPatLnk = 'Statement'
let g:VimrexSrchAncLnk = 'Identifier'
let g:VimrexSrchTokLnk = 'Constant'
let g:VimrexSrchCgpLnk = 'Type'
let g:VimrexSrchGrpLnk = 'Comment'
let g:VimrexSrchExpLnk = 'Underlined'
let g:VimrexSrchChcLnk = 'Special'
let g:VimrexFilePatLnk = 'PreProc'

" multicursor quit
let multicursor_quit = '-pq'
nnore <leader>pp :<c-u>call MultiCursorPlaceCursor()<cr>
nnore <leader>pm :<c-u>call MultiCursorManual()<cr>
nnore <leader>pc :<c-u>call MultiCursorRemoveCursors()<cr>
xnore <leader>pp :<c-u>call MultiCursorVisual()<cr>
nnore <leader>p/ :<c-u>call MultiCursorSearch('')<cr>

" Paster:
let PASTER_BROWSER_COMMAND = '/usr/bin/open'

" Lisp coloring of parens
let g:lisp_rainbow = 1

" XPTemplate:
" Set personal snippet folder location:
"
let g:xptemplate_snippet_folders=['$HOME/.vim/xptemplate_personal_snippets']
"
" Turn off automatic closing of quotes and braces:
"
let g:xptemplate_brace_complete = 0
"
" Snippet triggering key:
let g:xptemplate_key = '<Tab>'
"
" Open the pop-up menu:
let g:xptemplate_key_pum_only = '<Leader><Tab>'
"
" Clear current placeholder and jump to the next:
"imap <C-d> <Tab>
let g:xptemplate_nav_cancel = '<C-d>'
"
" Move to the next placeholder in a snippet:
let g:xptemplate_nav_next = '<Tab>'
"
" Go to the end of the current placeholder and in to insert mode:
"
" <C-_> is actually CONTROL-/ on my keyboard.
let g:xptemplate_to_right = '<C-_>'
"
" Move cursor back to last placeholder:
let g:xptemplate_goback = '<C-g>'
"
" Use TAB/S-TAB to navigate through the pop-up menu:
let g:xptemplate_pum_tab_nav = 1
"
let vimpager_use_gvim = 1

" Highlight trailing spaces with Solarized
let g:solarized_visibility="high"    "default value is normal
if !has('gui_running')
  let solarized_termcolors = 256
endif
"let g:solarized_hitrail=1    "default value is 0

let g:jellybeans_use_lowcolor_black = 1

" Signify
if !has('gui_running') && $TERM_PROGRAM == 'iTerm.app'
  let signify_sign_weight = 'NONE'
endif
"let g:signify_vcs_list = ['git', 'hg', 'bzr']
let g:signify_vcs_list = ['git', 'hg']
"let g:signify_sign_color_inherit_from_linenr = 1
let g:signify_cursorhold_normal = 0
let g:signify_cursorhold_insert = 0
let g:signify_line_highlight = 0

" Startify
let g:startify_custom_header =
      \ map(
      \   map(
      \     split(system('fortune -a | cowsay'), '\n')
      \     , '"   ". v:val'),
      \   'substitute(v:val, "[[:cntrl:]]", "", "g")')
      \ + ['','']
let g:startify_list_order = ['files', 'dir', 'bookmarks', 'sessions']
let g:startify_bookmarks = ['~/.vimrc']
let g:startify_change_to_vcs_root = 1
let g:startify_skiplist = [
      \ 'COMMIT_EDITMSG',
      \ '.*/doc',
      \ $VIMRUNTIME . '/doc',
      \ '.viminfo',
      \ '.DS_Store',
      \ ]

" Undotree:
let undotree_DiffCommand = 'diff -u'
let undotree_WindowLayout = 2
let undotree_TreeNodeShape = 'o'
let undotree_DiffpanelHeight = 15

" Nofrils
let g:nofrils_strbackgrounds = 1
let g:nofrils_heavycomments = 1

""""""""""""

" Functions {{{1
""""""""""""
let s:fn_idx = 0
function! Fn(fn_form)
  let [fn_args, fn_body] = split(a:fn_form, '\s*=>\s*')
  let s:fn_idx = s:fn_idx + 1
  let fname = 'Fn_' . s:fn_idx
  let b_elems = split(fn_body, '|')
  let b_elems[-1] = 'return ' . b_elems[-1]
  exe 'func! ' . fname . fn_args . "\n"
        \. join(b_elems, "\n") . "\n"
        \. 'endfunc'
  return function(fname)
endfunction

function! Fx(fn, ...)
  return call(a:fn, a:000)
endfunction

" Help terms can include characters typically not considered within keywords.
function! ExpandHelpWord()
  let iskeyword = &iskeyword
  set iskeyword=!-~,^),^*,^\|,^",192-255
  let word = expand('<cword>')
  let &iskeyword = iskeyword
  return word
endfunction

function! s:SynNames()
  let syn = {}
  let lnum = line('.')
  let cnum = col('.')
  let [effective, visual] = [synID(lnum, cnum, 0), synID(lnum, cnum, 1)]
  let syn.effective = synIDattr(effective, 'name')
  let syn.effective_link = synIDattr(synIDtrans(effective), 'name')
  let syn.visual = synIDattr(visual, 'name')
  let syn.visual_link = synIDattr(synIDtrans(visual), 'name')
  return syn
endfunction

function! s:SynInfo()
  let syn = s:SynNames()
  let info = ''
  if syn.visual != ''
    let info .= printf('visual: %s', syn.visual)
    if syn.visual != syn.visual_link
      let info .= printf(' (as %s)', syn.visual_link)
    endif
  endif
  if syn.effective != syn.visual
    if syn.visual != ''
      let info .= ', '
    endif
    let info .= printf('effective: %s', syn.effective)
    if syn.effective != syn.effective_link
      let info .= printf(' (as %s)', syn.effective_link)
    endif
  endif
  return info
endfunction

function! MyOwnFoldText()
  let line = getline(v:foldstart)
  let sub = substitute(line, '/\*\|\*/\|{{{\d\=', '', 'g')
  return v:folddashes . sub
endfunction

function! GoFile()
  try
    normal! gf
  catch /^Vim\%((\a\+)\)\=:E447/
    let q = substitute(v:exception,'^Vim\%((\a\+)\)\=:E\d\+: \(.*\)','\1','')
          \ . ', would you like to create it?'
    try
      buffer <cfile>
    catch /^Vim\%((\a\+)\)\=:E94/
      if confirm(q ,"&Yes\n&No\n&Cancel",3) == 1
        exec 'edit '.expand('%:p:h').'/'.expand('<cfile>')
      endif
    endtry
  endtry
endfunction

" https://gist.github.com/romainl/047aca21e338df7ccf771f96858edb86
" make list-like commands more intuitive
function! CCR()
    let cmdline = getcmdline()
    if cmdline =~ '\v\C^(ls|files|buffers)'
        " like :ls but prompts for a buffer command
        return "\<CR>:b"
    elseif cmdline =~ '\v\C/(#|nu|num|numb|numbe|number)$'
        " like :g//# but prompts for a command
        return "\<CR>:"
    elseif cmdline =~ '\v\C^(dli|il)'
        " like :dlist or :ilist but prompts for a count for :djump or :ijump
        return "\<CR>:" . cmdline[0] . "j  " . split(cmdline, " ")[1] . "\<S-Left>\<Left>"
    elseif cmdline =~ '\v\C^(cli|lli)'
        " like :clist or :llist but prompts for an error/location number
        return "\<CR>:sil " . repeat(cmdline[0], 2) . "\<Space>"
    elseif cmdline =~ '\C^old'
        " like :oldfiles but prompts for an old file to edit
        set nomore
        return "\<CR>:sil se more|e #<"
    elseif cmdline =~ '\C^changes'
        " like :changes but prompts for a change to jump to
        set nomore
        return "\<CR>:sil se more|norm! g;\<S-Left>"
    elseif cmdline =~ '\C^ju'
        " like :jumps but prompts for a position to jump to
        set nomore
        return "\<CR>:sil se more|norm! \<C-o>\<S-Left>"
    elseif cmdline =~ '\C^marks'
        " like :marks but prompts for a mark to jump to
        return "\<CR>:norm! `"
    elseif cmdline =~ '\C^undol'
        " like :undolist but prompts for a change to undo
        return "\<CR>:u "
    else
        return "\<CR>"
    endif
endfunction

"""""""""""
" Commands {{{1
"""""""""""

" Super retab
:command! -range=% -nargs=0 Tab2Space
      \ execute "<line1>,<line2>s/^\\t\\+/\\=substitute(submatch(0), '\\t',"
      \ . " repeat(' ', ".&ts."), 'g')"
:command! -range=% -nargs=0 Space2Tab execute "<line1>,<line2>s/^\\( \\{"
      \ . &ts . "\\}\\)\\+/\\=substitute(submatch(0), ' \\{"
      \ . &ts . "\\}', '\\t', 'g')"

" Toggle soft-wrap:
"noremap <silent> <Leader>w :call ToggleWrap()<CR>
command! -nargs=* Wrap set wrap linebreak nolist
command! -nargs=* NoWrap set nowrap nolinebreak list
function! ToggleWrap()
  if &wrap
    echo "Wrap OFF"
    setlocal nowrap
    "set virtualedit=all
    silent! nunmap <buffer> <Up>
    silent! nunmap <buffer> <Down>
    silent! nunmap <buffer> <Home>
    silent! nunmap <buffer> <End>
    silent! nunmap <buffer> k
    silent! nunmap <buffer> j
    silent! nunmap <buffer> 0
    silent! nunmap <buffer> $
    silent! iunmap <buffer> <Up>
    silent! iunmap <buffer> <Down>
    silent! iunmap <buffer> <Home>
    silent! iunmap <buffer> <End>
  else
    echo "Wrap ON"
    setlocal wrap linebreak nolist
    "set virtualedit=
    setlocal display+=lastline
    noremap  <buffer> <silent> <Up>   gk
    noremap  <buffer> <silent> <Down> gj
    noremap  <buffer> <silent> <Home> g<Home>
    noremap  <buffer> <silent> <End>  g<End>
    inoremap <buffer> <silent> <Up>   <C-o>gk
    inoremap <buffer> <silent> <Down> <C-o>gj
    inoremap <buffer> <silent> <Home> <C-o>g<Home>
    inoremap <buffer> <silent> <End>  <C-o>g<End>
    noremap  <buffer> <silent> k gk
    noremap  <buffer> <silent> j gj
    noremap  <buffer> <silent> 0 g0
    noremap  <buffer> <silent> $ g$
  endif
endfunction

" Reverse lines.
command! -nargs=0 -bar -range=% Reverse <line2>kz<bar><line1>,<line2>g/^/m'z

if has('gui_macvim')
  command! -nargs=+ Google exec '!open ''http://www.google.com/search?q='
        \ .substitute('<args>', '\s','+','g')."&'"
endif

command! Vimrc e ~/.vimrc

command! -nargs=? Test exec 'e ~/playground/test.' . (empty(<q-args>) ? 'vim' : <q-args>)

" Various {{{1
""""""""""
"colorscheme paradigm_currentterm
"colorscheme desert
"colorscheme off
"colorscheme railscasts
"colorscheme alduin
"colorscheme blues
"colorscheme distill
colorscheme aquamarine

" vim: set et sw=2:{{{1
